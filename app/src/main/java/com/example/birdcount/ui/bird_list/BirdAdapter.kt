package com.example.birdcount.ui.bird_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.birdcount.R
import com.example.birdcount.databinding.ItemBirdBinding
import com.example.birdcount.model.Bird

class BirdAdapter: RecyclerView.Adapter<BirdViewHolder>() {

    private val birds = mutableListOf<Bird>()

    fun setBirds(birds: List<Bird>){
        this.birds.clear()
        this.birds.addAll(birds)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BirdViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bird, parent, false)
        return BirdViewHolder(view)
    }

    override fun onBindViewHolder(holder: BirdViewHolder, position: Int) {
        val bird = birds[position]
        holder.bind(bird)
    }

    override fun getItemCount(): Int = birds.count()
}

class BirdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(bird: Bird) {
        val binding = ItemBirdBinding.bind(itemView)
        binding.itemBirdLocation.text = bird.location
        binding.itemBirdColor.text = bird.color
        binding.itemBirdTime.text = bird.timeAdded.toString()
    }
}