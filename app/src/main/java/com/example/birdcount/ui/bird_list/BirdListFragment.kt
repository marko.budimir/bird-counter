package com.example.birdcount.ui.bird_list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.birdcount.databinding.FragmentBirdListBinding
import com.example.birdcount.di.BirdRepositoryFactory


class BirdListFragment : Fragment() {

    private lateinit var binding: FragmentBirdListBinding
    private lateinit var adapter: BirdAdapter
    private val birdRepository = BirdRepositoryFactory.birdRepository

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBirdListBinding.inflate(layoutInflater)
        setupRecyclerView()
        binding.fabAddBird.setOnClickListener { showCreateNewBirdFragment() }
        return binding.root
    }



    private fun setupRecyclerView() {
        binding.rvBirdList.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        adapter = BirdAdapter()
        binding.rvBirdList.adapter = adapter
    }

    private fun showCreateNewBirdFragment() {
        val action = BirdListFragmentDirections.actionBirdListFragmentToNewBirdFragment()
        findNavController().navigate(action)
    }

    override fun onResume() {
        super.onResume()
        updateData()
    }

    private fun updateData() {
        adapter.setBirds(birdRepository.getAllBirds())
    }



}