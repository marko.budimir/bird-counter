package com.example.birdcount.ui.bird_new

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.birdcount.databinding.FragmentNewBirdBinding
import com.example.birdcount.di.BirdRepositoryFactory
import com.example.birdcount.model.Bird
import com.example.birdcount.utils.getDate


class NewBirdFragment : Fragment() {
    private val birdRepository = BirdRepositoryFactory.birdRepository
    lateinit var binding: FragmentNewBirdBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewBirdBinding.inflate(layoutInflater)
        binding.tpTimeAddedInput.setIs24HourView(true)
        binding.dpDateAddedInput.maxDate = System.currentTimeMillis()
        binding.btnAddBird.setOnClickListener{saveBird()}
        return binding.root
    }

    private fun saveBird() {
        val location = binding.etBirdLocationInput.text.toString()
        val color = binding.etBirdColorInput.text.toString()
        val timePicker = binding.tpTimeAddedInput
        val dateAdded = binding.dpDateAddedInput.getDate(timePicker.hour, timePicker.minute)

        birdRepository.save(Bird(0, location, color, dateAdded))

        Toast.makeText(context, "Saving bird", Toast.LENGTH_SHORT).show()
        val action = NewBirdFragmentDirections.actionNewBirdFragmentToBirdListFragment()
        findNavController().navigate(action)
    }



}