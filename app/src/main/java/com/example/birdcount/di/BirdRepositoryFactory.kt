package com.example.birdcount.di

import com.example.birdcount.BirdCounterApp
import com.example.birdcount.data.BirdRepository
import com.example.birdcount.data.BirdRepositoryImpl
import com.example.birdcount.data.room.BirdDatabase

object BirdRepositoryFactory {

    val roomDb = BirdDatabase.getDatabase(BirdCounterApp.application)
    val birdRepository: BirdRepository = BirdRepositoryImpl(roomDb.getBirdDao())
}