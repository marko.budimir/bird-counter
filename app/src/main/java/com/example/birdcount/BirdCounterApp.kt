package com.example.birdcount

import android.app.Application

class BirdCounterApp : Application() {
    companion object {
        lateinit var application: Application
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }
}