package com.example.birdcount.data

import com.example.birdcount.model.Bird

interface BirdRepository {

    fun save(bird: Bird)
    fun delete(bird: Bird)
    fun getBirdById(id: Long): Bird?
    fun getAllBirds(): List<Bird>
}