package com.example.birdcount.data

import com.example.birdcount.model.Bird

class BirdRepositoryImpl(val birdDao: BirdDao): BirdRepository {
    override fun save(bird: Bird) = birdDao.save(bird)
    override fun delete(bird: Bird) = birdDao.delete(bird)
    override fun getBirdById(id: Long): Bird? = birdDao.getBirdById(id)
    override fun getAllBirds(): List<Bird> = birdDao.getAllBirds()
}