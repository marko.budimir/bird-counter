package com.example.birdcount.data

import androidx.room.*
import com.example.birdcount.model.Bird

@Dao
interface BirdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(bird: Bird)

    @Delete
    fun delete(bird: Bird)

    @Query("SELECT * FROM birds WHERE id =:id")
    fun getBirdById(id: Long): Bird?

    @Query("SELECT * FROM birds")
    fun getAllBirds(): List<Bird>
}