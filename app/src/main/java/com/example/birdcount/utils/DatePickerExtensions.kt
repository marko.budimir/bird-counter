package com.example.birdcount.utils

import android.widget.DatePicker
import java.util.*

fun DatePicker.getDate(hour:Int, minute: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.set(year, month, dayOfMonth, hour, minute, 0)
    return calendar.time
}