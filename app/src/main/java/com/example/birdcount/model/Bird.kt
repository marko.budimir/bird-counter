package com.example.birdcount.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "birds")
data class Bird(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    @ColumnInfo(name = "location")
    val location: String,
    @ColumnInfo(name = "color")
    val color: String,
    @ColumnInfo(name = "timeAdded")
    val timeAdded: Date
)
